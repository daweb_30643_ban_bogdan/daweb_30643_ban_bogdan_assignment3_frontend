import React from 'react';
import './home-container.css';
import {Button} from "reactstrap";


class HomeContainer extends React.Component {

    handleClick = () => {
        localStorage.setItem('language','en');
        window.location.reload(false)
    }

    handleClick1 = () => {
        localStorage.setItem('language','ro')
        window.location.reload(false)
    }

    render(){
        let lang = localStorage.getItem('language')
        return(
            <>
        {lang === 'en' ?
        <div className="header">
            <h1>
                Welcome to DentalWEB
            </h1>
            <Button onClick={this.handleClick}>english</Button>
            <Button onClick={this.handleClick1}>romanian</Button>
        </div>
        :
        <div className="header">
            <h1>
                Bine ati venit la DentalWEB
            </h1>
            <Button onClick={this.handleClick}>english</Button>
            <Button onClick={this.handleClick1}>romanian</Button>
        </div>}
        </>
        );
    }

}

export default HomeContainer;