import React from 'react';
import './contact-container.css'
import {Button, Modal, ModalBody, ModalHeader} from "reactstrap";
import MailForm from "./mail-form";

class ContactContainer extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            selected: false,
            collapseForm: false,
            tableData: [],
            isLoaded: false,
            errorStatus: 0,
            error: null
        };

        this.toggleForm = this.toggleForm.bind(this);
        this.reload = this.reload.bind(this);
    }

    toggleForm() {
        this.setState({selected: !this.state.selected});
    }

    reload() {
        this.setState({
            isLoaded: false
        });
        this.toggleForm();
    }

    render(){
        let lang = localStorage.getItem('language')
        return(
            <>
                {lang==='ro'?
            <div className={'contact'}>
                <ul>
                    <li className={'title'}><h1>Contact</h1></li>
                    <li className={'contact-item'}>Adresa: Strada Castanelor 8</li>
                    <li className={'contact-item'}>Telefon: 0261 856 139</li>
                    <li className={'contact-item'}>Email: dentalWeb2021@gmail.com </li>
                    <Button onClick={this.toggleForm}>Trimite un mail </Button>
                </ul>
            </div>
                    :
            <div className={'contact'}>
                <ul>
                    <li className={'title'}><h1>Contact</h1></li>
                    <li className={'contact-item'}>Address: Street Castanelor 8</li>
                    <li className={'contact-item'}>Telephone: 0261 856 139</li>
                    <li className={'contact-item'}>Email: dentalWeb2021@gmail.com </li>
                    <Button onClick={this.toggleForm}>Send a mail </Button>
                </ul>
            </div>}

            <Modal isOpen={this.state.selected} toggle={this.toggleForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleForm}> Compose a mail </ModalHeader>
                    <ModalBody>
                        <MailForm reloadHandler={this.reload}/>
                    </ModalBody>
            </Modal>
            </>
        );
    }
}

export default ContactContainer;