import React from "react";
import {Button, Form, Input} from "antd";
import {CalendarOutlined, UserOutlined} from "@ant-design/icons";
import axios from "axios";
import {connect} from "react-redux";


class AppointmentContainer extends React.Component{

    constructor() {
        super();
        this.state = {
            is_auth: null
        }
    }

    componentWillReceiveProps(newProps) {
        console.log(newProps.token)
        this.setState({is_auth:newProps.token})
    }

    onFinish = (values) => {
        console.log('Received values of form: ', values);
        console.log(values.pacient_name);
        console.log(values.doctor_name);
        axios.post('http://127.0.0.1:8000/api/apps',{
            pacient_name: values.pacient_name,
            app_date: values.app_date,
            doctor_name: values.doctor_name
        },
            {headers:{Authorization: 'Bearer ' + this.state.is_auth}});
        this.props.history.push('/');
    }

    render(){
        return(
            <div>
                {this.state.is_auth === null ? <h1>Permission denied</h1> :
                    <div>
                <h2>Create appointment</h2>
                <Form name="normal_login" className="login-form"
                initialValues={{
                    remember: true,
                }}
                onFinish = {this.onFinish}>
                <Form.Item name="pacient_name"
                    rules={[
                        {
                            required: true,
                            message: 'Please input your the pacient\'s name'
                        },
                    ]}>
                    <Input prefix={<UserOutlined className="site-form-item-icon"/>} placeholder="Pacient name"/>
                </Form.Item>

                <Form.Item name="app_date"
                    rules={[
                        {
                            required: true,
                            message: 'Please input the date of the appointment'
                        },
                    ]}>
                    <Input prefix={<CalendarOutlined className="site-form-item-icon"/>} placeholder="Appointment date"/>
                </Form.Item>

                <Form.Item name="doctor_name"
                    rules={[
                        {
                            required: true,
                            message: 'Please input the doctor\'s name'
                        }
                    ]}>
                    <Input
                        prefix={<UserOutlined className="site-form-item-icon"/>}
                        placeholder="Doctor name"
                    />
                </Form.Item>

                <Form.Item>
                    <Button type="primary" htmlType="submit" className="makeapp-form-but">
                        Create appointment
                    </Button>
                </Form.Item>
                </Form>
            </div>
                    }
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        token: state.token
    }

}

export default connect(mapStateToProps)(AppointmentContainer);