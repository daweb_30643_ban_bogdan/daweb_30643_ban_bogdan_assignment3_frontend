import React from 'react';
import axios from "axios";
import {connect} from "react-redux";
import {Card, CardText, Col, Label, Row} from "reactstrap";


class DetailsContainer extends React.Component{

    constructor() {
        super();

        this.state = {
            table_data:{},
            is_auth: null
        }
    }

    componentDidMount() {
        let id = localStorage.getItem('current_user_id');
        console.log("user id = ",id);
        this.fetchDetails(id);
    }

    componentWillReceiveProps(newProps) {
        console.log(newProps.token)
        this.setState({is_auth:newProps.token})
    }


    fetchDetails(id){
        axios.get("http://127.0.0.1:8000/api/userr/"+id)
            .then(res=> {
                console.log(res.data);
                this.setState({table_data: res.data});
            });
    }

    render(){
        return(
            <>
            {this.state.is_auth === null ? <h1>Permission denied</h1> :
                <div>
                    Profile
                    <br/>
                    <br/>
                    <Card title="profile">
                        <Label>Name:</Label> {this.state.table_data.name},<br/>
                        Email: {this.state.table_data.email},<br/>
                        Medical services: {this.state.table_data.medical_services}<br/>
                    </Card>
                </div>
            }
            </>
        );
    }
}
const mapStateToProps = state => {
    return {
        token: state.token
    }

}

export default connect(mapStateToProps)(DetailsContainer);