import React from 'react';
import {List,Card} from 'antd';


const DoctorPage = (props) => {
    return(
        <List
        grid={{
          gutter: 16,
          xs: 1,
          sm: 2,
          md: 4,
          lg: 4,
          xl: 6,
          xxl: 3,
        }}
        dataSource={props.data}
        renderItem={item => (
          <List.Item>
            <Card title={<img src={item.poza} alt="poza" width="80" height="80" />}>
                Nume: {item.nume},<br/>
                Prenume: {item.prenume},<br/>
                Specializare: {item.specializare}
                <br/>
                <br/>
                <br/>
            </Card>
              {/*<Link to={{pathname: `/api/details/${item.id}`}}>*/}
              {/*    <Button className="mybutton">  Details </Button>*/}
              {/*</Link>*/}
              {/*<Link to={{pathname: `/api/update/${item.id}`}}>*/}
              {/*    <Button className="mybutton">  Update </Button>*/}
              {/*</Link>*/}
          </List.Item>
        )}
      />
    );
}
export default DoctorPage;