import React from 'react';
import {List,Card} from 'antd';

const DoctorPage2 = (props) => {
    return(
        <List
        grid={{
          gutter: 16,
          xs: 1,
          sm: 2,
          md: 4,
          lg: 4,
          xl: 6,
          xxl: 3,
        }}
        dataSource={props.data}
        renderItem={item => (
          <List.Item>
            <Card title={<img src={item.poza} alt="poza" width="80" height="80" />}>
                Surname: {item.nume},<br/>
                First name: {item.prenume},<br/>
                Specialization: {item.specializare}
                <br/>
                <br/>
                <br/>
            </Card>
          </List.Item>
        )}
      />
    );
}
export default DoctorPage2;